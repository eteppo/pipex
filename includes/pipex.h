/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   pipex.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/14 14:59:43 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/06 19:44:11 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <errno.h>

typedef struct s_pipex
{
	int		i;
	int		infile;
	int		outfile;
	char	**my_paths;
	int		pipe_end[2];
	pid_t	child1;
	pid_t	child2;
}				t_pipex;

// -------------------- Mandatory ------------------------
// main.c
int		monitor_child_processes(t_pipex pipex);
void	create_child_processes(t_pipex *pipex, char **envp, char **argv);
int		main(int argc, char **argv, char **envp);

// -------------------- Shared ------------------------
// error.c
void	perror_and_exit(char *str, int error);

// execute.c
char	*get_command_file(t_pipex pipex, char *cmd_arg);
char	**get_command(char **argv, int index);
char	*get_command_path(t_pipex pipex, char **cmd);
void	execute_command(t_pipex *pipex, char **envp, char **argv, int index);

// parse.c
int		save_paths(char **envp, t_pipex *pipex);

// split_pipex.c
char	**free_array(char **array);
size_t	str_counter(char const *s, char c);
char	**malloc_array(size_t count, char const *s, char c, int slash);
char	**fill_array(char const *s, char **ptr_array, char c, int slash);
char	**split_pipex(char const *s, char c, int slash);

// utils.c
size_t	ft_strlen(const char *str);
char	*ft_strjoin(char const *s1, char const *s2);
int		ft_strncmp(const char *s1, const char *s2, size_t n);

#endif