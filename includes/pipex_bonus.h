/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   pipex_bonus.h                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/04/06 19:13:05 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/06 19:38:12 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_BONUS_H
# define PIPEX_BONUS_H

# include "pipex.h"

// ---------------------- Bonus ----------------------
// main_bonus.c
void	execute_cmd(t_pipex *pipex, char *argv_i, char **envp);
void	go_to_fork(char *cmd, char **envp, t_pipex *pipex);
int		main(int argc, char **argv, char **envp);

#endif
