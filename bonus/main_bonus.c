/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main_bonus.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/08 12:26:18 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/07 10:01:05 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex_bonus.h"

void	execute_cmd(t_pipex *pipex, char *argv_i, char **envp)
{
	char	**cmd;
	char	*cmd_path;

	cmd = split_pipex(argv_i, ' ', 0);
	if (!cmd)
		perror_and_exit("Execute - Saving cmd fails", EXIT_FAILURE);
	cmd_path = get_command_file(*pipex, cmd[0]);
	if (!cmd_path)
		perror_and_exit("Execute - Command not found", 127);
	if (access(cmd_path, X_OK) == -1)
		perror_and_exit("Execute - No permission to execute", EXIT_FAILURE);
	execve(cmd_path, cmd, envp);
	free(cmd_path);
	cmd_path = NULL;
	free(cmd);
	cmd = NULL;
	perror_and_exit("Execute - Execve errors", EXIT_FAILURE);
}

void	go_to_fork(char *cmd, char **envp, t_pipex *pipex)
{
	pipex->child1 = fork();
	if (pipex->child1 < 0)
		perror_and_exit("Fork failed", errno);
	else if (!pipex->child1)
	{
		close(pipex->pipe_end[0]);
		if (dup2(pipex->pipe_end[1], STDOUT_FILENO) < 0)
			perror_and_exit("Redirecting stdout fails", errno);
		close(pipex->pipe_end[1]);
		execute_cmd(pipex, cmd, envp);
	}
	waitpid(pipex->child1, NULL, 0);
	close(pipex->pipe_end[1]);
	if (dup2(pipex->pipe_end[0], STDIN_FILENO) < 0)
		perror_and_exit("Redirecting stdin fails", errno);
	close(pipex->pipe_end[0]);
}

int	main(int argc, char **argv, char **envp)
{
	t_pipex	pipex;

	pipex.i = 2;
	if (argc < 5)
		perror_and_exit("Main - Invalid argument count", EXIT_FAILURE);
	pipex.infile = open(argv[1], O_RDONLY);
	pipex.outfile = open(argv[argc - 1], O_CREAT | O_RDWR | O_TRUNC, 0644);
	if (pipex.infile < 0 || pipex.outfile < 0)
		perror_and_exit("Main - Opening file failed", EXIT_FAILURE);
	if (save_paths(envp, &pipex) == -1)
		perror_and_exit("Main - Saving paths errors", EXIT_FAILURE);
	if (dup2(pipex.infile, STDIN_FILENO) < 0)
		perror_and_exit("Main - Redirecting stdin fails", errno);
	if (dup2(pipex.outfile, STDOUT_FILENO) < 0)
		perror_and_exit("Main - Redirecting stdout fails", errno);
	while (pipex.i <= argc - 3)
	{
		pipe(pipex.pipe_end);
		if (!pipex.pipe_end[0] || !pipex.pipe_end[1])
			perror_and_exit("Error with creating pipe", EPIPE);
		go_to_fork(argv[pipex.i], envp, &pipex);
		pipex.i++;
	}
	execute_cmd(&pipex, argv[pipex.i], envp);
	return (EXIT_FAILURE);
}
