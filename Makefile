# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    Makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: eteppo <eteppo@student.codam.nl>             +#+                      #
#                                                    +#+                       #
#    Created: 2022/02/08 12:07:33 by eteppo        #+#    #+#                  #
#    Updated: 2022/04/07 09:58:37 by eteppo        ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

NAME = pipex

SRCS = main.c error.c execute.c parse.c split_pipex.c utils.c

SRCS_BONUS = main_bonus.c error.c execute.c parse.c split_pipex.c utils.c

VPATH = ./mandatory/ ./bonus/ ./shared/

OBJS_DIR = ./objs

CC = gcc

FLAGS = -Wall -Werror -Wextra -fsanitize=address

ifdef BONUS
OBJS = $(addprefix $(OBJS_DIR)/, $(SRCS_BONUS:.c=.o))
else
OBJS = $(addprefix $(OBJS_DIR)/, $(SRCS:.c=.o))
endif

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAGS) $(OBJS) -o $(NAME)

$(OBJS_DIR)/%.o: %.c
	$(CC) $(FLAGS) -c $< -o $@

clean:
	rm -f $(OBJS_DIR)/*.o

fclean: clean
	rm -f $(NAME)

bonus: clean_mandatory
	$(MAKE) BONUS=1 all

clean_mandatory:
	@[ -f $(OBJS_DIR)/main.o ] \
	&& rm -f $(OBJS_DIR)/main.o \
	|| true

re: fclean all

.PHONY: all clean fclean re bonus
