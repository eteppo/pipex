/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/08 12:26:18 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/07 09:57:38 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

int	monitor_child_processes(t_pipex pipex)
{
	int		wait_status;
	int		exit_status;

	wait_status = 0;
	exit_status = 0;
	if (waitpid(pipex.child1, &wait_status, 0) == -1)
		perror_and_exit("Waitpid1 failed", errno);
	if (WIFEXITED(wait_status))
		exit_status = WEXITSTATUS(wait_status);
	if (waitpid(pipex.child2, &wait_status, 0) == -1)
		perror_and_exit("Waitpid2 failed", errno);
	if (WIFEXITED(wait_status))
		exit_status = WEXITSTATUS(wait_status);
	return (exit_status);
}

void	create_child_processes(t_pipex *pipex, char **envp, char **argv)
{
	pipex->child1 = fork();
	if (pipex->child1 < 0)
		perror_and_exit("Fork1 failed", errno);
	else if (!pipex->child1)
	{
		close(pipex->pipe_end[0]);
		if (dup2(pipex->pipe_end[1], STDOUT_FILENO) < 0)
			perror_and_exit("Child1 - Redirecting stdout fails", errno);
		close(pipex->pipe_end[1]);
		close(pipex->infile);
		execute_command(pipex, envp, argv, 2);
	}
	pipex->child2 = fork();
	if (pipex->child2 < 0)
		perror_and_exit("Fork2 failed", errno);
	else if (!pipex->child2)
	{
		waitpid(pipex->child1, NULL, 0);
		close(pipex->pipe_end[1]);
		if (dup2(pipex->pipe_end[0], STDIN_FILENO) < 0)
			perror_and_exit("Child2 - Redirecting stdin fails", errno);
		close(pipex->pipe_end[0]);
		close(pipex->outfile);
		execute_command(pipex, envp, argv, 3);
	}
}

int	main(int argc, char **argv, char **envp)
{
	t_pipex	pipex;

	if (argc != 5)
		perror_and_exit("Main - Invalid argument count", EXIT_FAILURE);
	pipex.infile = open(argv[1], O_RDONLY);
	pipex.outfile = open(argv[4], O_CREAT | O_RDWR | O_TRUNC, 0644);
	if (pipex.infile < 0 || pipex.outfile < 0)
		perror_and_exit("Main - Opening file failed", EXIT_FAILURE);
	if (save_paths(envp, &pipex) == -1)
		perror_and_exit("Main - Saving paths errors", EXIT_FAILURE);
	if (dup2(pipex.infile, STDIN_FILENO) < 0)
		perror_and_exit("Child1 - Redirecting stdin fails", errno);
	if (dup2(pipex.outfile, STDOUT_FILENO) < 0)
		perror_and_exit("Child2 - Redirecting stdout fails", errno);
	pipe(pipex.pipe_end);
	if (!pipex.pipe_end[0] || !pipex.pipe_end[1])
		perror_and_exit("Main - Error with creating pipe", EPIPE);
	create_child_processes(&pipex, envp, argv);
	close(pipex.pipe_end[0]);
	close(pipex.pipe_end[1]);
	return (monitor_child_processes(pipex));
}
