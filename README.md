## The mandatory part

It must take 4 arguments:  

* file1 and file2 are file names.
* cmd1 and cmd2 are shell commands with their parameters.  
  
Will be executed as follows:  
``make && ./pipex file1 cmd1 cmd2 file2``  
  
Behaves exactly the same as the shell command below:  
``$> < file1 cmd1 | cmd2 > file2``  
  
## The bonus part

Handles multiple pipes

This:  
``make bonus && ./pipex file1 cmd1 cmd2 cmd3 ... cmdn file2``

Behaves like:  
``< file1 cmd1 | cmd2 | cmd3 ... | cmdn > file2``