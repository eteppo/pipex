/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   execute.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/04/06 19:10:04 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/11 13:09:55 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

char	*get_command_file(t_pipex pipex, char *cmd_arg)
{
	size_t	i;
	char	*cmd;

	i = 0;
	if (cmd_arg && !access(cmd_arg, F_OK))
		return (cmd_arg);
	if (pipex.my_paths)
	{
		while (pipex.my_paths[i])
		{
			cmd = ft_strjoin(pipex.my_paths[i], (const char *)cmd_arg);
			if (!cmd)
				perror_and_exit("Strjoin errors", EXIT_FAILURE);
			if (!access(cmd, F_OK))
				return (cmd);
			free(cmd);
			i++;
		}
	}
	return (NULL);
}

char	*get_command_path(t_pipex pipex, char **cmd)
{
	char	*cmd_path;

	cmd_path = get_command_file(pipex, cmd[0]);
	if (!cmd_path)
		perror_and_exit("Command not found", 127);
	if (access(cmd_path, X_OK) == -1)
		perror_and_exit("No permission to execute", EXIT_FAILURE);
	return (cmd_path);
}

char	**get_command(char **argv, int index)
{
	char	**cmd;

	cmd = split_pipex(argv[index], ' ', 0);
	if (!cmd)
		perror_and_exit("Get_command fails", EXIT_FAILURE);
	return (cmd);
}

void	execute_command(t_pipex *pipex, char **envp, char **argv, int index)
{
	char	*cmd_path;
	char	**cmd;

	cmd = get_command(argv, index);
	cmd_path = get_command_path(*pipex, cmd);
	execve(cmd_path, cmd, envp);
	free(cmd);
	cmd = NULL;
	free(cmd_path);
	cmd_path = NULL;
	perror_and_exit("Execution errors", EXIT_FAILURE);
}
