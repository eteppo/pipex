/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   error.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/25 11:17:37 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/06 10:19:24 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

void	perror_and_exit(char *str, int error)
{
	perror(str);
	exit(error);
}
