/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/15 11:32:40 by eteppo        #+#    #+#                 */
/*   Updated: 2022/04/11 12:51:23 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

int	save_paths(char **envp, t_pipex *pipex)
{
	int		i;
	char	*path_line;

	i = 0;
	path_line = NULL;
	while (envp[i])
	{
		if (ft_strncmp(envp[i], "PATH=", 5) == 0)
		{
			path_line = envp[i] + 5;
			pipex->my_paths = split_pipex(path_line, ':', 1);
			if (!pipex->my_paths)
				return (-1);
			break ;
		}
		i++;
	}
	return (0);
}
